.PHONT: todo exe

todo: main

exe: todo
	./main

Pool.o: Pool.cpp Pool.h
	g++ -std=c++11 -c -I/usr/local/include/SDL2 Pool.cpp -o Pool.o

main.o: main.cpp Pool.h
	g++ -std=c++11 -c -I/usr/local/include/SDL2 main.cpp -o main.o

main: main.o Pool.o
	g++ -L/usr/local/lib main.o Pool.o -o main -lSDL2 -lpthread -D_REENTRANT

