# README #

This is a simple Pool implementation using SDL2 threads.

To use it, first you need to have installed http://libsdl.org/download-2.0.php under:
/usr/local/include/SDL2
/usr/local/lib

Contains an example of use in the main.cpp file, to run it use:
make
./main